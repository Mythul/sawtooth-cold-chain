
REQUIREMENTS:

1. Node version: 6.12.3
2. NPM version: 3.10.10
3. SAWTOOTH SDK BUILD FROM -> BRANCH 0.8 (only this branch has certain new methods)

**BUILD:**

1) Build the client:

cd {project directory}/nodejs/client
npm install
npm run build

2) Run the main method of the Java Transaction Processor

**Docker:**

Remove all containers command:

docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)

Remove all images command:
docker system prune -a

Start docker after the genesis block has been initialized using the following command:

docker-compose -f sawtooth-default.yaml up

3) Accessing the blockchain:

docker exec -it sawtooth-client-default bash