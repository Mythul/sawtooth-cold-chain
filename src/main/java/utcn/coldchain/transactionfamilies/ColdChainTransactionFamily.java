package utcn.coldchain.transactionfamilies;


import sawtooth.sdk.processor.Utils;

import java.io.UnsupportedEncodingException;

public class ColdChainTransactionFamily {

    public static final String TRANSACTION_FAMILY_NAME = "cold-chain";
    public static final String TRANSACTION_FAMILY_VERSION = "0.1";
    public static final String TRANSACTION_FAMILY_ENCODING = "application/json";

    // By convention the address for an Organisation is built by hashing the id + name
    public static String getOrganisationDataAddress(String id, String name, String coldChainNamespace) throws UnsupportedEncodingException {
        final String organisationInputString = id + name;
        String organisationHash = Utils.hash512(organisationInputString.getBytes("UTF-8"));
        return coldChainNamespace + organisationHash.substring(0, 64);
    }
}
