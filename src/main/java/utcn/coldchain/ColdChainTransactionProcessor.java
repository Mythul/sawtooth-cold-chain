package utcn.coldchain;


import sawtooth.sdk.processor.TransactionProcessor;
import utcn.coldchain.handlers.ColdChainTransactionHandler;

// ---------------------
// Organisation - represents a basic node of the supply chain (eg. company, depot, shop)
//
// ---------------------
public class ColdChainTransactionProcessor {

    /**
     * START THE ORGANISATION TRANSACTION PROCESSOR
     **/
    public static void main(String[] args) {
        TransactionProcessor transactionProcessor = new TransactionProcessor("tcp://localhost:4004");
        transactionProcessor.addHandler(new ColdChainTransactionHandler());
        Thread thread = new Thread(transactionProcessor);
        thread.start();
    }
}
