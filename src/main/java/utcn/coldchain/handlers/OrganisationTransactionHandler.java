package utcn.coldchain.handlers;

import com.google.gson.Gson;
import com.google.protobuf.ByteString;
import org.apache.commons.lang3.StringUtils;
import sawtooth.sdk.processor.State;
import sawtooth.sdk.processor.exceptions.InternalError;
import sawtooth.sdk.processor.exceptions.InvalidTransactionException;
import utcn.coldchain.data.OrganisationData;
import utcn.coldchain.data.OrganisationType;
import utcn.coldchain.transactionfamilies.ColdChainTransactionFamily;
import utcn.coldchain.validators.OrganisationDataValidator;

import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.logging.Logger;

public class OrganisationTransactionHandler {

    private final Logger logger = Logger.getLogger(OrganisationTransactionHandler.class.getName());

    private OrganisationDataValidator organisationDataValidator;

    public OrganisationTransactionHandler() {
        this.organisationDataValidator = new OrganisationDataValidator();
    }

    // -----------------------------
    // ACTION IMPLEMENTATIONS
    // -----------------------------

    public void executeInitAction(String coldChainNamespace, State stateStore) throws UnsupportedEncodingException, InvalidTransactionException, InternalError {
        logger.info("OrganisationTransactionHandler - starting: execute init action");

        OrganisationData organisationData1 = new OrganisationData("O-1", "Tower Group", "1st Main Street", OrganisationType.MANUFACTURER);
        OrganisationData organisationData2 = new OrganisationData("O-2", "City Group", "22 Tower Street", OrganisationType.MEDICAL_CENTER);
        OrganisationData organisationData3 = new OrganisationData("D-1", "Big Depot", "2nd Bridge Street", OrganisationType.DEPOT);

        Collection<Map.Entry<String, ByteString>> addressValues = new ArrayList<>();
        addressValues.add(buildBlockchainOrganisationEntry(organisationData1, coldChainNamespace, stateStore));
        addressValues.add(buildBlockchainOrganisationEntry(organisationData2, coldChainNamespace, stateStore));
        addressValues.add(buildBlockchainOrganisationEntry(organisationData3, coldChainNamespace, stateStore));

        // SAVE the data in the blockchain
        Collection<String> addresses = stateStore.set(addressValues);

        if (addresses.size() < 1) {
            throw new InternalError("State Error");
        }
    }

    public void executeSaveAction(OrganisationData organisationData, String coldChainNamespace, State stateStore) throws UnsupportedEncodingException, InvalidTransactionException, InternalError {
        logger.info("OrganisationTransactionHandler - starting: execute save action");

        Collection<Map.Entry<String, ByteString>> addressValues = new ArrayList<>();
        addressValues.add(buildBlockchainOrganisationEntry(organisationData, coldChainNamespace, stateStore));

        // SAVE the data in the blockchain
        Collection<String> addresses = stateStore.set(addressValues);

        if (addresses.size() < 1) {
            throw new InternalError("State Error");
        }
    }

    private Map.Entry<String, ByteString> buildBlockchainOrganisationEntry(OrganisationData organisationData, String coldChainNamespace, State stateStore)
            throws UnsupportedEncodingException, InternalError, InvalidTransactionException {
        // Mandatory validation
        organisationDataValidator.validate(organisationData);

        // Get the hashed organisationAddress from the blockchain
        String addressOrganisationData = ColdChainTransactionFamily.getOrganisationDataAddress(organisationData.getId(), organisationData.getName(), coldChainNamespace);

        // Check for the address value in the blockchain state
        String existingAddressOrganisationData = stateStore.get(Collections.singletonList(addressOrganisationData)).get(addressOrganisationData).toStringUtf8();

        if (StringUtils.isNotEmpty(existingAddressOrganisationData)) {
            throw new InvalidTransactionException("Organisation " + organisationData.getName() + " address already exists in the blockchain.");
        }

        // Build the new data
        Gson gson = new Gson();

        String organisationDataJSON = gson.toJson(organisationData);
        ByteString organisationDataByteString = ByteString.copyFromUtf8(organisationDataJSON);
        Map.Entry<String, ByteString> entry = new AbstractMap.SimpleEntry<>(addressOrganisationData, organisationDataByteString);
        return entry;
    }
}
