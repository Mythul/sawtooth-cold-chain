package utcn.coldchain.handlers;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import sawtooth.sdk.processor.State;
import sawtooth.sdk.processor.TransactionHandler;
import sawtooth.sdk.processor.Utils;
import sawtooth.sdk.processor.exceptions.InternalError;
import sawtooth.sdk.processor.exceptions.InvalidTransactionException;
import sawtooth.sdk.protobuf.TpProcessRequest;
import utcn.coldchain.data.OrganisationData;
import utcn.coldchain.data.TransactionData;
import utcn.coldchain.handlers.actions.OrganisationTransactionAction;
import utcn.coldchain.transactionfamilies.ColdChainTransactionFamily;
import utcn.coldchain.validators.ColdChainPayloadValidator;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Logger;

public class ColdChainTransactionHandler implements TransactionHandler {

    private final Logger logger = Logger.getLogger(ColdChainTransactionHandler.class.getName());

    private String coldChainNamespace;

    private ColdChainPayloadValidator coldChainPayloadValidator;
    private OrganisationTransactionHandler organisationTransactionHandler;

    // -------------------
    // CONSTRUCTOR
    //
    // - processes namespace
    // -------------------

    public ColdChainTransactionHandler() {
        this.coldChainPayloadValidator = new ColdChainPayloadValidator();
        this.organisationTransactionHandler = new OrganisationTransactionHandler();

        try {
            this.coldChainNamespace = Utils.hash512(this.transactionFamilyName().getBytes("UTF-8")).substring(0, 6);
        } catch (UnsupportedEncodingException usee) {
            usee.printStackTrace();
            this.coldChainNamespace = "";
        }
    }

    // ---------------------------------------------
    // TRANSACTION HANDLER - FAMILY DESCRIPTION
    // ---------------------------------------------

    @Override
    public String transactionFamilyName() {
        return ColdChainTransactionFamily.TRANSACTION_FAMILY_NAME;
    }

    @Override
    public String getVersion() {
        return ColdChainTransactionFamily.TRANSACTION_FAMILY_VERSION;
    }

    @Override
    public Collection<String> getNameSpaces() {
        ArrayList<String> namespaces = new ArrayList<>();
        namespaces.add(this.coldChainNamespace);
        return namespaces;
    }

    @Override
    public String getEncoding() {
        return ColdChainTransactionFamily.TRANSACTION_FAMILY_ENCODING;
    }

    // ---------------------------------------------
    // TRANSACTION HANDLER - MAIN METHOD !
    // ---------------------------------------------

    @Override
    public void apply(TpProcessRequest tpProcessRequest, State stateStore) throws InvalidTransactionException, InternalError {
        // Get the signer public key
        String signer = tpProcessRequest.getHeader().toStringUtf8();

        // build the transaction data
        Gson gson = new Gson();
        TransactionData transactionData = getUnpackedTransactionData(tpProcessRequest);

        // mandatory validation
        coldChainPayloadValidator.validate(transactionData);

        switch (transactionData.getAction()) {

            case OrganisationTransactionAction.ORGANISATION_INIT_ACTION: {
                try {
                    organisationTransactionHandler.executeInitAction(coldChainNamespace, stateStore);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                break;
            }

            case OrganisationTransactionAction.ORGANISATION_SAVE_ACTION: {
                try {
                    OrganisationData organisationData = gson.fromJson(transactionData.getPayload(), OrganisationData.class);
                    organisationTransactionHandler.executeSaveAction(organisationData, coldChainNamespace, stateStore);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // ---------------------------------------------
    // TRANSACTION HANDLER - BUSINESS LOGIC METHODS
    // ---------------------------------------------

    private TransactionData getUnpackedTransactionData(TpProcessRequest transactionRequest) {
        // Get the payload which is in application/json format
        Gson gson = new Gson();
        JsonParser parser = new JsonParser();

        JsonObject coldChainTransactionDataJSON = parser.parse(transactionRequest.getPayload().toStringUtf8()).getAsJsonObject();

        TransactionData transactionData = gson.fromJson(coldChainTransactionDataJSON.get("transactionData"), TransactionData.class);
        return transactionData;
    }
}
