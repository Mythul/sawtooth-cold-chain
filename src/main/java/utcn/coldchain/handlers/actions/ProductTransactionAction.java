package utcn.coldchain.handlers.actions;

public class ProductTransactionAction {

    /*
        This action creates 3 initial Products
        It needs to be run only once.
    */
    public static final String PRODUCT_INIT_ACTION = "product-init-action";
}
