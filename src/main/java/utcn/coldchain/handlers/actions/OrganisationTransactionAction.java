package utcn.coldchain.handlers.actions;

// -----------------------------------
//
// Possible actions for OrganisationData
//
// -----------------------------------
public class OrganisationTransactionAction {

    /*
        This action creates 3 initial Organisations
        It needs to be run only once.
     */
    public static final String ORGANISATION_INIT_ACTION = "organisation-init-action";

    /*
        This action saves a new Organisation
    */
    public static final String ORGANISATION_SAVE_ACTION = "organisation-save-action";

}
