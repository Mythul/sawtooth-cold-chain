package utcn.coldchain.handlers;

import utcn.coldchain.validators.ProductDataValidator;

import java.util.logging.Logger;

public class ProductTransactionHandler {

    private final Logger logger = Logger.getLogger(ProductTransactionHandler.class.getName());

    private ProductDataValidator productDataValidator;

    public ProductTransactionHandler() {
        this.productDataValidator = new ProductDataValidator();
    }

    // -----------------------------
    // ACTION IMPLEMENTATIONS
    // -----------------------------
}
