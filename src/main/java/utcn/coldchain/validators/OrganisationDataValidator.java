package utcn.coldchain.validators;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.commons.lang3.StringUtils;
import sawtooth.sdk.processor.exceptions.InvalidTransactionException;
import utcn.coldchain.data.OrganisationData;

public class OrganisationDataValidator implements SimpleValidator<OrganisationData> {

    @Override
    public void validate(JsonObject transactionData) throws InvalidTransactionException {
        Gson gson = new Gson();
        OrganisationData organisationData = gson.fromJson(transactionData, OrganisationData.class);
        this.validate(organisationData);
    }

    @Override
    public void validate(OrganisationData organisationData) throws InvalidTransactionException {
        if (organisationData == null) {
            throw new InvalidTransactionException("COLD CHAIN - OrganisationData is null.");
        }

        if (StringUtils.isEmpty(organisationData.getId())) {
            throw new InvalidTransactionException("COLD CHAIN - OrganisationData id is empty/null.");
        }

        if (StringUtils.isEmpty(organisationData.getName())) {
            throw new InvalidTransactionException("COLD CHAIN - OrganisationData name is empty/null.");
        }

        if (StringUtils.isEmpty(organisationData.getAddress())) {
            throw new InvalidTransactionException("COLD CHAIN - OrganisationData address is empty/null.");
        }
    }
}
