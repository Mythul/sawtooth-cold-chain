package utcn.coldchain.validators;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.commons.lang3.StringUtils;
import sawtooth.sdk.processor.exceptions.InvalidTransactionException;
import utcn.coldchain.data.TransactionData;

public class ColdChainPayloadValidator implements SimpleValidator<TransactionData> {

    public void validate(JsonObject transactionDataJSON) throws InvalidTransactionException {
        Gson gson = new Gson();
        TransactionData transactionData = gson.fromJson(transactionDataJSON, TransactionData.class);
        this.validate(transactionData);
    }

    @Override
    public void validate(TransactionData transactionData) throws InvalidTransactionException {
        if (transactionData == null) {
            throw new InvalidTransactionException("COLD CHAIN - Transaction Data is null.");
        }

        // A transaction data action is mandatory
        if (StringUtils.isEmpty(transactionData.getAction())) {
            throw new InvalidTransactionException("COLD CHAIN - Missing action value on the transaction data payload.");
        }
    }
}
