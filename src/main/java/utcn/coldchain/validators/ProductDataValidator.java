package utcn.coldchain.validators;

import com.google.gson.JsonObject;
import sawtooth.sdk.processor.exceptions.InvalidTransactionException;
import utcn.coldchain.data.ProductData;

public class ProductDataValidator implements SimpleValidator<ProductData> {
    @Override
    public void validate(JsonObject transactionDataJSON) throws InvalidTransactionException {

    }

    @Override
    public void validate(ProductData productData) throws InvalidTransactionException {
        if (productData == null) {
            throw new InvalidTransactionException("COLD CHAIN - ProductData is null.");
        }
    }
}
