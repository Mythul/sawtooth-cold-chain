package utcn.coldchain.validators;

import com.google.gson.JsonObject;
import sawtooth.sdk.processor.exceptions.InvalidTransactionException;

public interface SimpleValidator<T> {

    void validate(JsonObject transactionDataJSON) throws InvalidTransactionException;

    void validate(T transactionData) throws InvalidTransactionException;
}
