package utcn.coldchain.data;

public class ProductData {

    private String id;
    private String name;
    private OrganisationData organisation;

    public ProductData(String id, String name, OrganisationData organisation) {
        this.id = id;
        this.name = name;
        this.organisation = organisation;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OrganisationData getOrganisation() {
        return organisation;
    }

    public void setOrganisation(OrganisationData organisation) {
        this.organisation = organisation;
    }
}
