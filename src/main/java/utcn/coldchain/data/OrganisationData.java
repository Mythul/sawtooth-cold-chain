package utcn.coldchain.data;

public class OrganisationData {

    private String id;
    private String name;
    private String address;
    private OrganisationType type;

    public OrganisationData(String id, String name, String address, OrganisationType type) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public OrganisationType getType() {
        return type;
    }

    public void setType(OrganisationType type) {
        this.type = type;
    }
}
