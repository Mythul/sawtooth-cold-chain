package utcn.coldchain.data;

import com.google.gson.JsonObject;

public class TransactionData {

    private String action;
    private JsonObject payload;

    public TransactionData(String action, JsonObject payload) {
        this.action = action;
        this.payload = payload;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public JsonObject getPayload() {
        return payload;
    }

    public void setPayload(JsonObject payload) {
        this.payload = payload;
    }
}
