package utcn.coldchain.data;

public enum OrganisationType {

    DEPOT("depot"),
    MANUFACTURER("manufacturer"),
    MEDICAL_CENTER("medical center"),
    TRANSPORTER("transporter");

    private String typeName;

    OrganisationType(String typeName) {
        this.typeName = typeName;
    }

    public String getTypeName() {
        return typeName;
    }
}
