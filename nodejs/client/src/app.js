'use strict';

const $ = require('jquery');

// Functionality
const {
    getKeys,
    makeKeyPair,
    saveKeys,
    getState,
    submitUpdate
} = require('./state');

const {
    addOption,
    addRow,
    addAction
} = require('./components');

const concatNewOwners = (existing, ownerContainers) => {
    return existing.concat(ownerContainers
        .filter(({owner}) => !existing.includes(owner))
        .map(({owner}) => owner))
};

// Application Object
const app = {user: null, keys: [], organisations: []};

// ------------------------------------
// Get the state of the blockchain
// ------------------------------------
app.refresh = function () {
    getState(({organisations}) => {
        this.organisations = organisations;

        // Clear existing data views
        $('#organisationList').empty();
        // $('[name="assetSelect"]').children().slice(1).remove();

        // Populate asset views
        organisations.forEach(organisation => {
            addRow('#organisationList', organisation.id, organisation.name, organisation.address, organisation.type);
        });

        // Populate transfer select with both local and blockchain keys
        let publicKeys = this.keys.map(pair => pair.public);
        // publicKeys = concatNewOwners(publicKeys, organisations);
        // publicKeys.forEach(key => addOption('[name="transferSelect"]', key))
    })
};

app.update = function (transactionData, owner) {
    if (this.user) {
        submitUpdate(
            {transactionData, owner},
            this.user.private,
            success => success ? this.refresh() : null
        )
    }
};

// Select User
$('[name="keySelect"]').on('change', function () {
    if (this.value === 'new') {
        app.user = makeKeyPair();
        app.keys.push(app.user);
        saveKeys(app.keys);
        addOption(this, app.user.public, true);
        addOption('[name="transferSelect"]', app.user.public)
    } else if (this.value === 'none') {
        app.user = null
    } else {
        app.user = app.keys.find(key => key.public === this.value)
        app.refresh()
    }
});

// Init Organisations Actions
$('#organisation-init-action-submit').on('click', function () {
    const action = $('#organisation-init-action').val();
    if (action) {
        const transactionData = {};
        transactionData.action = action;
        transactionData.payload = {};

        // Execute
        app.update(transactionData);
    }
});

// Transfer Asset
$('#transferSubmit').on('click', function () {
    const asset = $('[name="assetSelect"]').val()
    const owner = $('[name="transferSelect"]').val()
    if (asset && owner) app.update('transfer', asset, owner)
});

// Accept Asset
$('#transferList').on('click', '.accept', function () {
    const asset = $(this).prev().text()
    if (asset) app.update('accept', asset)
})

$('#transferList').on('click', '.reject', function () {
    const asset = $(this).prev().prev().text()
    if (asset) app.update('reject', asset)
})

// Initialize
app.keys = getKeys()
app.keys.forEach(pair => addOption('[name="keySelect"]', pair.public))
app.refresh()
